using System;
using System.Collections;
using UnityEngine;

namespace PlayableEntity.Controllers
{
    [RequireComponent(typeof(Unit))]
    public class AIInput : MonoBehaviour
    {
        [SerializeField] private LayerMask layerToSkip;
        [SerializeField] private EnemyState originalState;

        [Header("Detection distances")] [SerializeField]
        private Vector2 obstacleDetectionOffset = new Vector2(3, 0);
        [SerializeField] private Vector2 playerDetectionOffset = new Vector2(3f, 0);

        [SerializeField] private float obstacleDetectionDistance = 2f;
        [SerializeField] private float attackDistance = 1f;
        [SerializeField] private float attackTriggerDistance = 4f;
        [SerializeField] private float backAttackTriggerDistance = 1f;

        [Header("Points for patrol and stay")] [SerializeField, Tooltip("Specify only this if in Stay mode")]
        private Transform pointA;

        [SerializeField] private Transform pointB;

        [Header("Cool downs")] [SerializeField]
        private float timeToWaitInPatrol = 3f;

        private WaitForSeconds attackCoolDown;

        private Unit enemyUnit;
        private EnemyState currentState;
        private Unit chaseTarget;

        private bool isPatrolStarted;
        private bool isAttacking;

        private void Start()
        {
            TryGetComponent(out enemyUnit);
            attackCoolDown = new WaitForSeconds(enemyUnit.PlayableStats.AttackCooldown);
            enemyUnit.OnDie += () =>
            {
                StopAllCoroutines();
            };
            if (pointA == null)
            {
                pointA = transform;
            };
        }

		private void OnEnable()
		{
            isAttacking = false;
        }

		private void Update()
        {
            if (enemyUnit.PlayableStats.HP.Current <= 0)
                return;

            PlayerSearch();
            Act();
        }

        private void PlayerSearch()
        {
            var characterDirection = enemyUnit.PlayableView.GetCharacterFlip() ? Vector2.left : Vector2.right;
            var rayOrigin = enemyUnit.transform.position + new Vector3(characterDirection.x * playerDetectionOffset.x,
                 playerDetectionOffset.y);
            var backRayOrigin = enemyUnit.transform.position +
                                new Vector3(-(characterDirection.x * playerDetectionOffset.x), playerDetectionOffset.y);
            
            Debug.DrawRay(rayOrigin, characterDirection * attackTriggerDistance, Color.red);
            RaycastHit2D playerInfo = Physics2D.Raycast(rayOrigin, characterDirection, attackTriggerDistance);
            
            Debug.DrawRay(backRayOrigin, -characterDirection * backAttackTriggerDistance, Color.red);
            RaycastHit2D playerInfoBack = Physics2D.Raycast(backRayOrigin, -characterDirection, backAttackTriggerDistance);
            
            var isPlayerFound = playerInfo.collider == true && playerInfo.collider.CompareTag("Player");
            var isPlayerInBack = playerInfoBack.collider == true && playerInfoBack.collider.CompareTag("Player");
            if (isPlayerFound || isPlayerInBack)
            {
                if (playerInfo.collider != null && playerInfo.collider.TryGetComponent(out chaseTarget) == false)
                {
                    chaseTarget = null;
                }
                if (playerInfoBack.collider != null && playerInfoBack.collider.TryGetComponent(out chaseTarget) == false)
                {
                    chaseTarget = null;
                }
            }
            else
            {
                chaseTarget = null;
            }

            currentState = chaseTarget ? EnemyState.Chase : originalState;
        }
        
        private bool ChecksWallAndGaps()
        {
            // Draw raycast in front and on the ground (to avoid gaps and walls)
            var characterDirection = enemyUnit.PlayableView.GetCharacterFlip() ? Vector2.left : Vector2.right;
            var obstacleOrigin = enemyUnit.transform.position +
                                 new Vector3(characterDirection.x * obstacleDetectionOffset.x,  obstacleDetectionOffset.y);

            Debug.DrawRay(obstacleOrigin, Vector2.down * obstacleDetectionDistance, Color.yellow);
            Debug.DrawRay(obstacleOrigin, characterDirection * obstacleDetectionDistance, Color.yellow);

            RaycastHit2D groundInfo = Physics2D.Raycast(obstacleOrigin, Vector2.down, obstacleDetectionDistance);

            RaycastHit2D wallInfo = Physics2D.Raycast(obstacleOrigin, characterDirection, obstacleDetectionDistance);

            // If gap or a wall is in front - set back to original state.
            var isNearGap = groundInfo.collider == false;
            var isNearWall = wallInfo.collider == true &&
                             wallInfo.collider.gameObject.layer == LayerMask.NameToLayer("Unit") == false;
            if (isNearGap || isNearWall)
            {
                return false;
            }

            return true;
        }

        private void Act()
        {
            Vector3 direction;
            float directionX;
            switch (currentState)
            {
                case EnemyState.Patrol:

                    // Go to point A stay there and then to point B
                    if (isPatrolStarted) break;
                    isPatrolStarted = true;
                    StartCoroutine(PatrolRoutine());
                    break;

                case EnemyState.Chase:
                    // Chase player and once near - attack
                    if (Mathf.Abs(enemyUnit.PlayableView.transform.position.x -
                        chaseTarget.PlayableView.transform.position.x) <= attackDistance)
                    {
                        if (isAttacking) break;
                        isAttacking = true;
                        StartCoroutine(AttackRoutine());
                        break;
                    }

                    if (ChecksWallAndGaps() == false)
                    {
                        currentState = originalState;
                        break;
                    }

                    directionX = chaseTarget.transform.position.x -
                                 enemyUnit.PlayableView.transform.position.x;
                    direction = new Vector3(Mathf.Clamp(directionX, -1, 1), 0);
                    enemyUnit.Move(direction.x);
                    break;

                case EnemyState.Stay:
                    
                    // If your coordinates are almost at point A - do nothing.
                    if (Mathf.Abs(enemyUnit.PlayableView.transform.position.x - pointA.position.x) <= 1f)
                        break;
                    // Get to point A  
                    directionX = pointA.transform.position.x - enemyUnit.PlayableView.transform.position.x;
                    direction = new Vector3(Mathf.Clamp(directionX, -1, 1), 0);
                    enemyUnit.Move(direction.x);
                    break;
            }
        }

        private IEnumerator AttackRoutine()
        {
            yield return attackCoolDown;
            enemyUnit.Attack();
            isAttacking = false;
        }

        private IEnumerator PatrolRoutine()
        {
            var currentTarget = pointA;
            
            // Remove while loop from here 
            while (currentState == EnemyState.Patrol)
            {
                // If distance till point A or B is close - wait few seconds and then switch target and switch target
                if (Mathf.Abs(enemyUnit.PlayableView.transform.position.x - currentTarget.position.x) <= 1f)
                {
                    yield return new WaitForSeconds(timeToWaitInPatrol);
                    currentTarget = currentTarget == pointA ? pointB : pointA;
                }
                else
                {
                    var directionX = currentTarget.transform.position.x -
                                     enemyUnit.PlayableView.transform.position.x;
                    var direction = new Vector3(Mathf.Clamp(directionX, -1, 1), 0);
                    enemyUnit.Move(direction.x);
                }

                yield return new WaitForEndOfFrame();
            }
            
            isPatrolStarted = false;
        }
    }

    enum EnemyState
    {
        Patrol,
        Chase,
        Stay,
    }
}