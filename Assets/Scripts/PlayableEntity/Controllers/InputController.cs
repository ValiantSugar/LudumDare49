using System.Collections;
using UnityEngine;

namespace PlayableEntity.Controllers
{
    public class InputController : MonoBehaviour
    {
        [SerializeField] private Unit playerUnit;
		[SerializeField] private KeyCode attackKeyCode;
		[SerializeField] private KeyCode secondaryAttackKeyCode;
		[SerializeField] private KeyCode jumpKeyCode;
		[SerializeField] private KeyCode blockKeyCode;
		[SerializeField] private KeyCode secondaryBlockKeyCode;
		[SerializeField] private KeyCode interactKeyCode;
		[SerializeField] private KeyCode pauseKeyCode;

		private WaitForSeconds attackCooldown;
		private bool isReadyToAttack = true;

		private void Start()
		{
			attackCooldown = new WaitForSeconds(playerUnit.PlayableStats.AttackCooldown);
		}

		private void Update()
		{
			if (playerUnit.PlayableStats.HP.Current <= 0)
				return;

			if ((Input.GetKeyDown(attackKeyCode) || Input.GetKeyDown(secondaryAttackKeyCode)) && isReadyToAttack)
				StartCoroutine(AttackRoutine());

			if (Input.GetKeyDown(jumpKeyCode))
				playerUnit.Jump();

			if (Input.GetKeyDown(blockKeyCode) || Input.GetKeyDown(secondaryBlockKeyCode))
				playerUnit.Block();
			else if (Input.GetKeyUp(blockKeyCode) || Input.GetKeyUp(secondaryBlockKeyCode))
				playerUnit.Unblock();

			if (Input.GetKeyDown(interactKeyCode))
				playerUnit.Interact();

			if (Input.GetKeyDown(pauseKeyCode))
			{
				GlobalGameStateManager.Instance.SwitchToPauseState();
			}

			var horizontalValue = Input.GetAxis("Horizontal");
			if (horizontalValue != 0)
				playerUnit.Move(horizontalValue);
		}

		private IEnumerator AttackRoutine()
		{
			isReadyToAttack = false;
			playerUnit.Attack();
			yield return attackCooldown;
			isReadyToAttack = true;
		}
	}
}
