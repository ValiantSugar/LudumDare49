using UnityEngine;
using Utilities;

namespace PlayableEntity
{
    [RequireComponent(typeof(Unit))]
    public class PlayerDontDestroy : Singleton<PlayerDontDestroy>
    {
        [HideInInspector] public Unit playerUnit;

        private void Awake()
        {
            TryGetComponent(out playerUnit);
        }
    }
}
