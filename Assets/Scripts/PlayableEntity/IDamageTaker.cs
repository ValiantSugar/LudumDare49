using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayableEntity
{
    public interface IDamageTaker
    {
        void TakeDamage(float damage, DirectionalSideType type = DirectionalSideType.None);
    }
}
