using System.Collections;
using System.Collections.Generic;
using PlayableEntity.Data;
using UnityEngine;
using Utilities;

namespace PlayableEntity.Components
{
    public class HealthComponent
    {
        private PlayableStatsSO playableStatsSo;

        public bool IsAlive => playableStatsSo.HP.Current > 0;

        public HealthComponent(PlayableStatsSO playableStatsSo)
        {
	        this.playableStatsSo = playableStatsSo;
        }

        public bool TakeDamage(float damage)
		{
			playableStatsSo.HP.Set(playableStatsSo.HP.Current - damage);
            Debug.Log($"Damage dealt: {damage}. Current health: {playableStatsSo.HP.Current}");
            return playableStatsSo.HP.Current <= 0;
		}
    }
}
