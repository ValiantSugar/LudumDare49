﻿using PlayableEntity.Data;
using UnityEngine;

namespace PlayableEntity.Components
{
    public class MovementComponent
    {
        private float speed;
        private Rigidbody2D rb2d;
        private Transform[] feetPoints;
        private PlayableViewComponent playableViewComponent;
        private bool isOnGround = true;
        private float currentXValue, currentYValue;
        private bool isCanJump;
        
        public MovementComponent(PlayableStatsSO playableStatsSo, PlayableViewComponent pvc, Rigidbody2D rb, Transform[] feet)
        {
            speed = playableStatsSo.Speed;
            isCanJump = playableStatsSo.IsPlayer;
            playableViewComponent = pvc;
            rb2d = rb;
            feetPoints = feet;
        }

        public void Move(float xValue)
        {
            currentXValue = xValue * speed;
            playableViewComponent.FlipCharacter(currentXValue < 0);
        }

        public void Jump(float currentJumpForce)
        {
            if(!isOnGround) return;
            currentYValue = currentJumpForce;
        }

        public void OnUpdate()
        {
            isOnGround = false;
            foreach (var feetPoint in feetPoints)
            {
                isOnGround = Physics2D.Raycast(feetPoint.position, Vector2.down, 0.15f);
                if (isOnGround)
                    break;
            }

            rb2d.velocity = new Vector2(currentXValue, rb2d.velocity.y + currentYValue);
            currentXValue = 0;
            currentYValue = 0;
            
            playableViewComponent.MoveAnimation(Mathf.Abs(rb2d.velocity.x));
            if(isCanJump)
                playableViewComponent.JumpAnimation(rb2d.velocity.y);
        }
    }
}