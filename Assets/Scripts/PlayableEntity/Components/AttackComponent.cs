using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace PlayableEntity.Components
{
    public class AttackComponent
    {
        private Vector2 offset;
        private Vector2 boxCastSize;
        private FloatStat damage;
        private IDamageTaker exceptionalDamageTaker;

        public AttackComponent(Vector2 offset, Vector2 boxCastSize, FloatStat damage, IDamageTaker ownerDamageTaker)
		{
            this.offset = offset;
            this.boxCastSize = boxCastSize;
            this.damage = damage;
            exceptionalDamageTaker = ownerDamageTaker;
        }

        public void Attack(DirectionalSideType sideType, Vector2 ownerPosition)
		{
            var originPosition = sideType == DirectionalSideType.Left ? ownerPosition - offset : ownerPosition + offset;
            var component = Physics2D.OverlapBox(originPosition, boxCastSize, 0f, 1 << 3);
            if (component != null && component.TryGetComponent(out IDamageTaker damageTaker) && damageTaker != exceptionalDamageTaker)
                damageTaker.TakeDamage(damage.Current, sideType);
		}
    }
}
