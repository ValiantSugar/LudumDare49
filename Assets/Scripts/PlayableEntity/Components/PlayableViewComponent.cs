﻿using UnityEngine;

namespace PlayableEntity.Components
{
    public class PlayableViewComponent : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer spriteRenderer;
        [SerializeField] private Animator animatorController;

        private static readonly int SpeedName = Animator.StringToHash("SpeedX");
        private static readonly int AttackTriggerName = Animator.StringToHash("AttackTrigger");
        private static readonly int BlockBoolName = Animator.StringToHash("IsBlocking");
        private static readonly int JumpName = Animator.StringToHash("JumpSpeed");
        private static readonly int DieBoolName = Animator.StringToHash("IsDead");

        public void FlipCharacter(bool flipX) => spriteRenderer.flipX = flipX;

        public bool GetCharacterFlip() => spriteRenderer.flipX;

        public void MoveAnimation(float xVelocity)
        {
            if (!animatorController) return;
            animatorController.SetFloat(SpeedName, xVelocity);
        }

        public void AttackAnimation()
        {
            if (!animatorController) return;
            animatorController.SetTrigger(AttackTriggerName);
        }

        public void BlockAnimation(bool isBlocking)
        {
            if (!animatorController) return;
            animatorController.SetBool(BlockBoolName, isBlocking);
        }

        public void JumpAnimation(float velocityYMagnitude)
        {
            if (!animatorController) return;
            animatorController.SetFloat(JumpName, velocityYMagnitude);
        }

        public void DieAnimation()
        {
            if (!animatorController) return;
            animatorController.SetTrigger(DieBoolName);
        }

        public void Restart()
		{
            if (!animatorController) return;
            animatorController.Play("Idle");
        }
    }
}