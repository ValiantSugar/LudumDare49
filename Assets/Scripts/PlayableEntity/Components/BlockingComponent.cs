using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayableEntity.Components
{
    public class BlockingComponent
    {
        private DirectionalSideType blockSideType;

        public DirectionalSideType GetBlockType => blockSideType;

        public void Block(DirectionalSideType type)
		{
            blockSideType = type;
        }

        public void Unblock()
		{
            blockSideType = DirectionalSideType.None;
        }
    }
}
