using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayableEntity
{
	public class Memento
	{
		public Vector3 StartPosition;

		public Memento(Vector3 startPosition)
		{
			StartPosition = startPosition;
		}
	}
}
