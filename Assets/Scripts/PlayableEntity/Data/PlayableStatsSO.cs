﻿using UnityEngine;
using Utilities;

namespace PlayableEntity.Data
{
    [CreateAssetMenu(fileName = "PlayableStats", menuName = "Playable Units/New playable unit", order = 0)]
    public class PlayableStatsSO : ScriptableObject
    {
        public bool IsPlayer;
        public float Speed;
        public float AttackCooldown;
        public FloatStat HP;
        public FloatStat Damage;
        public FloatStat JumpForce;
        public FloatStat BlockAmount;
        
        public Vector2 AttackOffset;
        public Vector2 BoxCastSize;

        public PlayableStatsSO DeepClone()
		{
            var newStats = new PlayableStatsSO();
            newStats.IsPlayer = IsPlayer;
            newStats.Speed = Speed;
            newStats.AttackCooldown = AttackCooldown;
            newStats.HP = HP.GetClone();
            newStats.HP.Reset();
            newStats.Damage = Damage.GetClone();
            newStats.JumpForce = JumpForce.GetClone();
            newStats.BlockAmount = BlockAmount.GetClone();
            newStats.AttackOffset = AttackOffset;
            newStats.BoxCastSize = BoxCastSize;
            return newStats;
        }
    }

    public enum StatName
    {
        Block,
        Jump,
        Attack,
        HP,
    }
}