using PlayableEntity.Components;
using PlayableEntity.Data;
using System;
using PlayableEntity.Data;
using UnityEngine;

namespace PlayableEntity
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class Unit : MementoOwner, IDamageTaker
    {

        public PlayableStatsSO PlayableStats;
        [HideInInspector] public PlayableViewComponent PlayableView;

        [SerializeField] private Transform[] feet;

        private HealthComponent healthComponent;
        private AttackComponent attackComponent;
        private BlockingComponent blockingComponent;
        private MovementComponent movementComponent;

        private Collider2D collider2D;
        private Rigidbody2D rb2d;

        public event Action OnDie;
        public event Action<Unit> OnInteract;
        public event Action<float> OnTakeDamage;

		private void Awake()
		{
            PlayableStats = PlayableStats.DeepClone();
        }

		private void Start()
		{
            TryGetComponent(out collider2D);
            TryGetComponent(out PlayableView);
            TryGetComponent(out rb2d);
            movementComponent = new MovementComponent(PlayableStats, PlayableView, rb2d, feet);

            SetComponents();
        }

		private void Update()
		{
            movementComponent.OnUpdate();
        }

		private void SetComponents()
		{
            healthComponent = new HealthComponent(PlayableStats);
            attackComponent = new AttackComponent(PlayableStats.AttackOffset, PlayableStats.BoxCastSize, PlayableStats.Damage, this);
            blockingComponent = new BlockingComponent();
            TryGetComponent(out collider2D);
            TryGetComponent(out PlayableView);
            TryGetComponent(out rb2d);
        }

        public void TakeDamage(float damage, DirectionalSideType type = DirectionalSideType.None)
        {
            float amountOfDmg = damage;
            if (type != DirectionalSideType.None
                && blockingComponent.GetBlockType != DirectionalSideType.None
                && blockingComponent.GetBlockType != type)
            {
                amountOfDmg = damage - PlayableStats.BlockAmount.Current;
                if (amountOfDmg < 0) return;
            }

            if (!healthComponent.IsAlive)
                return;

            OnTakeDamage?.Invoke(damage);

            if (healthComponent.TakeDamage(amountOfDmg))
            {
                PlayableView.DieAnimation();
                collider2D.enabled = false;
                rb2d.isKinematic = true;
                OnDie?.Invoke();
                return;
            }
        }

        public void Attack()
		{
            var sideType = PlayableView.GetCharacterFlip() ? DirectionalSideType.Left : DirectionalSideType.Right;
            attackComponent.Attack(sideType, transform.position);
            PlayableView.AttackAnimation();
        }

        public void Block()
		{
            var sideType = PlayableView.GetCharacterFlip() ? DirectionalSideType.Left : DirectionalSideType.Right;
            blockingComponent.Block(sideType);
            PlayableView.BlockAnimation(true);
        }

        public void Unblock()
        {
            blockingComponent.Unblock();
            PlayableView.BlockAnimation(false);
        }

        public void Interact()
        {
            OnInteract?.Invoke(this);
            OnInteract = null;
        }

        public void Move(float horValue)
        {
            movementComponent.Move(horValue);
        }

        public void Jump()
        {
            movementComponent.Jump(PlayableStats.JumpForce.Current);
        }

        public override void Recover()
        {
            base.Recover();
            SetComponents();
            PlayableStats.HP.Reset();
            PlayableView.Restart();
            collider2D.enabled = true;
            rb2d.isKinematic = false;
            Debug.Log("Memento recovery");
        }
    }

    public enum DirectionalSideType
	{
        None,
        Left,
        Right
	}
}
