using UnityEngine;

namespace PlayableEntity
{
	[RequireComponent(typeof(BoxCollider2D))]
	public class TrainingDummy : MonoBehaviour, IDamageTaker
	{
		[SerializeField] private float hp = 13;
		public void TakeDamage(float damage, DirectionalSideType type = DirectionalSideType.None)
		{
			hp -= damage;
			if (hp <= 0)
			{
				//TODO: Add animation
				gameObject.SetActive(false);
			}
		}
	}
}
