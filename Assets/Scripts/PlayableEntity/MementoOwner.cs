using UnityEngine;

namespace PlayableEntity
{
    public abstract class MementoOwner : MonoBehaviour
    {
        [SerializeField] protected Memento memento;

        public void Remember()
		{
            memento = new Memento(transform.position);
        }

        public virtual void Recover()
		{
            transform.position = memento.StartPosition;
            gameObject.SetActive(true);
        }
    }
}
