using UnityEngine;
using Cinemachine;
using Utilities;
using System;
using System.Linq;

namespace Managers
{
    [RequireComponent(typeof(CinemachineBrain)), SingularBehaviour(true, true,true)]
    public class CameraController : Singleton<CameraController>
    {
        [SerializeField] private ShakeContainer[] shakeContainers;

        private Camera cachedCamera;
        private float timer;
        private float totalTimer;
        private float startPower;
        private CinemachineVirtualCamera currentVirtualCamera;
        private CinemachineBasicMultiChannelPerlin perlin;

		protected void Awake()
		{
            cachedCamera = GetComponent<Camera>();
        }

        public static void SetVirtualCamera(CinemachineVirtualCamera newVirtualCamera)
		{
            if (Instance.currentVirtualCamera != null)
                Instance.currentVirtualCamera.gameObject.SetActive(false);

            Instance.currentVirtualCamera = newVirtualCamera;
            Instance.currentVirtualCamera.gameObject.SetActive(true);
        }

		public static void Shake(ShakeType type)
		{
            var container = Instance.shakeContainers.FirstOrDefault(x => x.Type == type);
            Instance.Shake(container.Power, container.Timer);
		}

        public void Shake(float power, float timer)
        {
            perlin = currentVirtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
            perlin.m_AmplitudeGain = power;
            startPower = power;
            this.timer = timer;
            totalTimer = timer;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.T))
                Shake(ShakeType.Normally);

            if (timer <= 0)
                return;

            timer -= Time.deltaTime;
            perlin.m_AmplitudeGain = Mathf.Lerp(startPower, 0f, (1 - (timer / totalTimer)));
        }

        [Serializable]
        private class ShakeContainer
		{
            public ShakeType Type;
            public float Timer;
            public float Power;
        }
    }

    [Serializable]
    public enum ShakeType
	{
        Easily,
        Normally,
        Strongly
    }
}
