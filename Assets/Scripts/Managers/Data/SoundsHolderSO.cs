using System;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Managers.Data
{
    [CreateAssetMenu(fileName = "SoundsHolder", menuName = "Audio/New sounds holder", order = 1)]
    public class SoundsHolderSO : ScriptableObject
    {
        [SerializeField] private SoundsContainer[] containers;

        public AudioClip GetClip(SoundType type)
		{
            var container = containers.FirstOrDefault(x => x.type == type);
            return container.clips[Random.Range(0, container.clips.Length)];
		}

        [Serializable]
        private struct SoundsContainer
		{
            public SoundType type;
            public AudioClip[] clips;
		}
    }

    public enum SoundType
	{
        Jump,
        Attack,
        TakingDamage,
        Death
	}
}
