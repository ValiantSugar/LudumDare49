using System;
using System.Collections;
using UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utilities;

namespace Manager
{
	public class SceneLoadingHelper : Singleton<SceneLoadingHelper>
	{
		private const int endingSceneIndex = 6;

		private static int lastSceneIndex;

		private void Update()
		{
			if (Input.GetKeyDown(KeyCode.Y))
				LoadNextSceneAsyncAsAdditional(null);
		}

		public static void LoadMainMenu(Action<AsyncOperation> completeCallback = null)
		{
			Instance.StartCoroutine(LoadAdditionalAsyncSceneCoroutine(1, completeCallback));
		}

		public static void LoadEndingScene()
		{
			SceneManager.LoadScene(endingSceneIndex);
		}

		public static void LoadAdditionalSceneAsync(int sceneIndex, Action<AsyncOperation> completeCallback = null)
		{
			lastSceneIndex = SceneManager.GetActiveScene().buildIndex;
			Instance.StartCoroutine(LoadAdditionalAsyncSceneCoroutine(sceneIndex, completeCallback));
		}

		public static void LoadNextSceneAsyncAsAdditional(Action<AsyncOperation> completeCallback = null)
		{
			lastSceneIndex = SceneManager.GetActiveScene().buildIndex;
			Instance.StartCoroutine(LoadAdditionalAsyncSceneCoroutine(lastSceneIndex + 1, completeCallback));
		}

		private static IEnumerator LoadAdditionalAsyncSceneCoroutine(int sceneIndex, Action<AsyncOperation> completeCallback)
		{
			var loadingAsyncOperation = SceneManager.LoadSceneAsync(sceneIndex, LoadSceneMode.Additive);

			if (completeCallback != null)
				loadingAsyncOperation.completed += completeCallback;

			while (!loadingAsyncOperation.isDone)
				yield return loadingAsyncOperation;

			SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(sceneIndex));
		}

		public static void UnloadPreviousScene()
		{
			SceneManager.UnloadSceneAsync(lastSceneIndex);
		}
	}
}