using DG.Tweening;
using Managers.Data;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;
using Utilities;

namespace Managers
{
	public class AudioManager : Singleton<AudioManager>
	{
		public static string DefaultGameMusicName = "animal-in-woods";
		public const string BossThemeName = "boss-fight";

		[SerializeField] AudioMixer mainMixer;
		[SerializeField] private AudioSource musicSource;
		[SerializeField] private AudioSource soundsSource;
		[SerializeField] private AudioClip[] musicClips;
		[SerializeField] private SoundsHolderSO soundsHolder;
		[SerializeField] private float musicFadeDuration = 0.5f;
		

		private Sequence changeMusicSequence;

		protected void Awake()
		{
			musicSource.loop = true;
			musicSource.Play();
		}

		private void Start()
		{
			//UIManager.OnMasterSwitch += SwitchMasterVolume;
			//UIManager.OnMusicSwitch += SwitchMusicVolume;
			//UIManager.OnSoundSwitch += SwitchMusicVolume;
		}

		public static void TryPlaySound(SoundType type)
		{
			var clip = Instance.soundsHolder.GetClip(type);
			Instance.soundsSource.PlayOneShot(clip);
		}

		public static void TrySetMusic(string clipName)
		{
			var clip = Instance.musicClips.FirstOrDefault(x => x.name == clipName);
			if (clip == null)
			{
				Debug.LogError($"Not found music with clipName {clipName}");
				return;
			}

			Instance.SetMusic(clip);
		}

		private void StopMusic()
		{
			changeMusicSequence?.Kill();

			changeMusicSequence =
				DOTween.Sequence()
				.Append(musicSource.DOFade(0f, musicFadeDuration))
				.AppendCallback(musicSource.Stop)
				.Pause();
		}

		private void SetMusic(AudioClip clip)
		{
			StopMusic();

			changeMusicSequence.AppendCallback(() =>
			{
				musicSource.clip = clip;
				musicSource.Play();
			});

			changeMusicSequence.Append(musicSource.DOFade(1f, musicFadeDuration));
			changeMusicSequence.Play();
		}

		private void SwitchMasterVolume(bool isOn) => mainMixer.SetFloat("MasterVolume", isOn ? 0f : -80f);

		private void SwitchMusicVolume(bool isOn) => mainMixer.SetFloat("MusicVolume", isOn ? 0f : -80f);

		private void SwitchSoundVolume(bool isOn) => mainMixer.SetFloat("SoundVolume", isOn ? 0f : -80f);
	}
}
