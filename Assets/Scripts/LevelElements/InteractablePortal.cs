﻿using Cinemachine;
using Manager;
using Managers;
using PlayableEntity;
using UI;
using UnityEngine;
using UnityEngine.Events;

namespace LevelEntity
{
    public class InteractablePortal : InteractableBase
    {
        [SerializeField] private Transform absoluteEndPoint;
        [SerializeField] private UnityAction additionalActions;
        [SerializeField] private CinemachineVirtualCamera virtualCamera;
        [SerializeField] private bool isNeedLoadNextLevel;
        [SerializeField] private bool isLastPortalInTheGame;

		protected override void OnInteract(Unit unitInteracted)
        {
            if (absoluteEndPoint)
                unitInteracted.transform.position = absoluteEndPoint.position;
            
            // BUG: Can potentially cause bugs by not letting the player use portal anymore. May look into here.
            isInteractable = false;
            additionalActions?.Invoke();
            if (virtualCamera != null)
                CameraController.SetVirtualCamera(virtualCamera);

            if (!isNeedLoadNextLevel) return;
                SceneLoadingHelper.LoadNextSceneAsyncAsAdditional();
			PlayerDontDestroy.Instance.playerUnit.PlayableStats.HP.SetCurrentToMax();

            if (isLastPortalInTheGame)
			{
                Hider.Fade(true, 1f, 2f, () =>
                {
                    DontDestroyOnLoad(unitInteracted.gameObject);
                    SceneLoadingHelper.LoadEndingScene();
                });
			}
		}
	}
}