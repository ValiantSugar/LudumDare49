﻿using System;
using DG.Tweening;
using UnityEngine;

namespace LevelElements
{
    public class SelfDestructingPlatform : PlatformBase
    {
        [SerializeField] private float timeToDestroy;
        [SerializeField] private float timeToReappear = 1f;
        private SpriteRenderer spriteRenderer;
        private void Start()
        {
            TryGetComponent(out spriteRenderer);
        }

        protected override void OnPlatformEnter()
        {
            spriteRenderer.DOFade(0f, timeToDestroy).OnComplete(() =>
            {
                unitInside.transform.SetParent(null);
                gameObject.SetActive(false);
            });
        }

        protected override void OnPlatformLeave()
        {
            DOTween.Kill(spriteRenderer);
            DOVirtual.DelayedCall(timeToReappear, () =>
            {
                gameObject.SetActive(true);
                spriteRenderer.DOFade(1, timeToReappear);
            });
            
        }
    }
}