﻿using DG.Tweening;
using UnityEngine;

namespace LevelElements
{
    public class MovingPlatform : PlatformBase
    {
        [SerializeField] private Transform startPoint;
        [SerializeField] private Transform endPoint;
        [SerializeField] private float timeToReach;
        [SerializeField] private float timeOut;


        private void Start()
        {
            transform.position = startPoint.position;
            Sequence animationSequence = DOTween.Sequence();
            animationSequence.Append(transform.DOMove(endPoint.position, timeToReach).SetEase(Ease.OutSine));
            animationSequence.AppendInterval(timeOut);
            animationSequence.Append(transform.DOMove(startPoint.position, timeToReach).SetEase(Ease.OutSine));
            animationSequence.AppendInterval(timeOut);
            animationSequence.SetLoops(-1, LoopType.Restart);
        }
    }
}