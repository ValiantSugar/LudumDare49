﻿using System;
using PlayableEntity;
using UnityEngine;

namespace LevelElements
{
    public abstract class PlatformBase : MonoBehaviour
    {
        protected Unit unitInside;
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (!other.collider.TryGetComponent(out unitInside) || !unitInside.PlayableStats.IsPlayer) return;
            unitInside.transform.SetParent(transform);
            OnPlatformEnter();
        }

        private void OnCollisionExit2D(Collision2D other)
        {
            if (!other.collider.TryGetComponent(out unitInside) || !unitInside.PlayableStats.IsPlayer) return;
            unitInside.transform.SetParent(null);
            DontDestroyOnLoad(unitInside);
            OnPlatformLeave();
        }

        protected virtual void OnPlatformEnter()
        {
        }

        protected virtual void OnPlatformLeave()
        {
        }
    }
}