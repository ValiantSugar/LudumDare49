﻿using PlayableEntity;
using UnityEngine;

namespace LevelEntity
{
    public abstract class InteractableBase : MonoBehaviour
    {
        private const string PromptToShow = "Press E to interact";
        protected bool isInteractable;

		private void Awake()
		{
			isInteractable = true;
		}

		private void OnTriggerEnter2D(Collider2D other)
        {
            // TODO: Show the prompt 
            // UIManager.ShowPrompt();

            if(!isInteractable) return;
            if (!other.TryGetComponent(out Unit unit)) return;

            if (unit.PlayableStats.IsPlayer == false) return;
            unit.OnInteract += OnInteract;
        }

        private void OnTriggerExit2D(Collider2D other)
		{
            if (!other.TryGetComponent(out Unit unit)) return;

            if (unit.PlayableStats.IsPlayer == false) return;
            unit.OnInteract -= OnInteract;
        }

        protected abstract void OnInteract(Unit unitInteracted);
    }
}