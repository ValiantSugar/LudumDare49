﻿using PlayableEntity;
using UnityEngine;

namespace LevelElements
{
    public class DeadlyObject : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.TryGetComponent(out Unit unit)) return;
            unit.TakeDamage((int) unit.PlayableStats.HP.Max);
        }
    }
}