using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace UI
{
    public class Hider : Singleton<Hider>
    {
        [SerializeField] private Image image;

        private Tween tween;

        public static void Fade(bool @in, float delay = 1f, float fadeTime = 1, Action onDone = null)
        {
            if (delay == 0)
            {
                var colorTemp = Instance.image.color;
                colorTemp.a = @in ? 1f : 0f;
                Instance.image.color = colorTemp;
                return;
            }

            if (Instance.tween != null)
                Instance.tween.Kill();

            Instance.tween = Instance.image.DOFade(@in ? 1f : 0f, fadeTime).SetDelay(delay);
            Instance.tween.OnComplete(() =>
            {
                onDone?.Invoke();
            });
        }
    }
}
