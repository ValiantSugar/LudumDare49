using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.ParametersMetes
{
    [RequireComponent(typeof(Slider))]
    public class BossHPView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI levelNameText;
        private Slider slider;

        private void Awake()
        {
            TryGetComponent(out slider);
        }

        public void Init(float max, string lvlName)
        {
            slider.minValue = 0;
            slider.maxValue = max;
            UpdateView(max);
            levelNameText.SetText(lvlName);
        }

        public void UpdateView(float currentValue)
        {
            slider.value = currentValue;
        }
    }
}
