using LevelEntity;
using PlayableEntity.Data;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace UI.ParametersMetes
{
    [RequireComponent(typeof(Slider))]
    public class ParameterView : MonoBehaviour
    {
        [SerializeField] private StatName parameterToShow;
        [SerializeField] private TextMeshProUGUI parameterAmountText;
        private Slider slider;
        private FloatStat trackedStat;

        private void Start()
        {
            var playerUnitStats = InstabilityController.Instance.PlayerUnit.PlayableStats;
            TryGetComponent(out slider);
            switch (parameterToShow)
            {
                case StatName.Block:
                    trackedStat = playerUnitStats.BlockAmount;
                    break;
                case StatName.Jump:
                    trackedStat = playerUnitStats.JumpForce;
                    break;
                case StatName.Attack:
                    trackedStat = playerUnitStats.Damage;
                    break;
                case StatName.HP:
                    trackedStat = playerUnitStats.HP;
                    break;
            }

            trackedStat.OnChange += ShowChange;
            slider.minValue = trackedStat.GetMin();
            slider.maxValue = trackedStat.GetMax();
            ShowChange(trackedStat.Current);
        }

        private void ShowChange(float valueToShow)
        {
            slider.value = valueToShow;
            parameterAmountText.SetText(valueToShow.ToString("F1"));
        }
    }
}