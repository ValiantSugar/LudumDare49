using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Managers;
using TMPro;
using UnityEngine;

public class EndSceneController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI endGameTxt;
    [SerializeField] private TextMeshProUGUI creditsGameTxt;

    [SerializeField] private string endGameMusic;

    void Start()
    {
        AudioManager.TrySetMusic(endGameMusic);
        Sequence textAnimation = DOTween.Sequence();
        textAnimation.Append(endGameTxt.transform.DOScale(Vector3.one * 1.5f, 3f));
        textAnimation.Append(endGameTxt.DOFade(0, 3f));
        textAnimation.Append(creditsGameTxt.rectTransform.DOMoveY(2400, 30)).SetEase(Ease.InSine);
        textAnimation.AppendCallback(() => endGameTxt.SetText("Thank you for playing!"));
        textAnimation.Append(endGameTxt.DOFade(1, 3f));
        textAnimation.AppendInterval(4f);
        textAnimation.OnComplete(Application.Quit);
    }
}