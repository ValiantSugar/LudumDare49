using Manager;
using Managers;
using UnityEngine;

public class GlobalGameStateManager : Singleton<GlobalGameStateManager>
{
    [SerializeField] private GameState startState = GameState.MainMenu;
    [SerializeField] private GameObject menuUIObj;
    [SerializeField] private GameObject gameplayUIObj;
    [SerializeField] private GameObject playerObj;
    [SerializeField] private GameObject instabilityObj;

    private void Awake()
    {
        menuUIObj.SetActive(false);
        playerObj.SetActive(false);
        gameplayUIObj.SetActive(false);
        instabilityObj.SetActive(false);
        if (startState == GameState.MainMenu)
        {
            SceneLoadingHelper.LoadMainMenu();
            menuUIObj.SetActive(true);
            AudioManager.TrySetMusic("lets-play");
        }
    }

    public void StartTheGame()
    {
        menuUIObj.SetActive(false);
        gameplayUIObj.SetActive(true);
        instabilityObj.SetActive(true);
        SceneLoadingHelper.LoadNextSceneAsyncAsAdditional();
        SceneLoadingHelper.UnloadPreviousScene();
        playerObj.SetActive(true);
        AudioManager.TrySetMusic("animal-in-woods");

    }

    public void UnPause()
    {
        menuUIObj.SetActive(false);
        gameplayUIObj.SetActive(true);
        Time.timeScale = 1;
    }

    public void SwitchToPauseState()
    {
        menuUIObj.SetActive(true);
        gameplayUIObj.SetActive(false);
        Time.timeScale = 0;
    }

    public void ExitTheGame()
    {
        Application.Quit();
    }
}

public enum GameState
{
    Pause,
    MainMenu,
    Game,
}
