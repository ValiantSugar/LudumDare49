using PlayableEntity;
using UI.ParametersMetes;
using Utilities;

namespace UI
{
    public class UIManager : Singleton<UIManager>
    {
        public BossHPView MiniBossHpView;
        public BossHPView BossHpView;

        public void ShowBossHP()
        {
            MiniBossHpView.gameObject.SetActive(false);
            BossHpView.gameObject.SetActive(true);
        }
    }
}
