using PlayableEntity;
using System;
using UnityEngine;

namespace LevelEntity.Triggers
{
    public class BossFightTrigger : MonoBehaviour
    {
        public event Action OnTriggerEnter;

        private bool isActivate;

        public void Restart()
		{
            isActivate = false;
        }

        private void OnTriggerEnter2D(Collider2D collider)
        {
            if (isActivate || !collider.TryGetComponent(out Unit unit)
                || !unit.PlayableStats.IsPlayer)
                return;

            isActivate = true;
            OnTriggerEnter?.Invoke();
            gameObject.SetActive(false);
        }
    }
}
