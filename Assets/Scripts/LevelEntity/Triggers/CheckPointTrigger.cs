﻿using Cinemachine;
using Manager;
using Managers;
using PlayableEntity;
using System;
using UnityEngine;

namespace LevelEntity.Triggers
{
	public class CheckPointTrigger : MonoBehaviour
	{
		[SerializeField] private MementoOwner[] mementos;
		[SerializeField] private CinemachineVirtualCamera virtualCamera;
		[SerializeField] private bool needUnloadPreviousScene;
		[SerializeField] private GameObject leftWall;

		public event Action<CheckPointTrigger> OnTriggerEnter;

		private void Awake()
		{
			foreach (var memento in mementos)
				memento.Remember();

			virtualCamera.gameObject.SetActive(false);
		}

		public void MementosRecovery()
		{
			foreach (var memento in mementos)
				memento.Recover();
		}

		private void OnTriggerEnter2D(Collider2D collider)
		{
			if (!collider.TryGetComponent(out Unit unit) 
				|| !unit.PlayableStats.IsPlayer)
				return;

			OnTriggerEnter?.Invoke(this);
			CameraController.SetVirtualCamera(virtualCamera);
			if (needUnloadPreviousScene)
			{
				SceneLoadingHelper.UnloadPreviousScene();
				needUnloadPreviousScene = false;
			}

			if (leftWall != null)
				leftWall.gameObject.SetActive(true);
		}
	}
}