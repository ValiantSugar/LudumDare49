using System;
using System.Collections;
using System.Collections.Generic;
using Managers;
using UnityEngine;

namespace LevelEntity
{
    public class ParallaxEffectController : MonoBehaviour
    {
        [SerializeField] private bool isTrackingOfMouse;
        [SerializeField] private ParallaxContainer[] containers;
        private Transform cameraTransform;

        public void Start()
        {
            cameraTransform = CameraController.Instance.transform;
            
            for (var i = 0; i < containers.Length; i++)
			{
                containers[i].StartPosition = containers[i].Transform.position;
                containers[i].StartCameraPosition = cameraTransform.position;
            }
        }

        public void LateUpdate()
        {
            if (cameraTransform == null)
            {
                cameraTransform = CameraController.Instance.transform;
            }
            for (var i = 0; i < containers.Length; i++)
            {
                var position = isTrackingOfMouse ? Camera.main.ScreenToWorldPoint(Input.mousePosition) : cameraTransform.position - containers[i].StartCameraPosition;
                float newX = containers[i].StartPosition.x + position.x * containers[i].ParalaxEffectX;
                float newY = !isTrackingOfMouse ? containers[i].Transform.position.y : (containers[i].StartPosition.y + position.y) * containers[i].ParalaxEffectY;
                containers[i].Transform.position = new Vector3(newX, newY, containers[i].Transform.position.z);
            }
        }

        [Serializable]
        public struct ParallaxContainer
		{
            public Transform Transform;
            public float ParalaxEffectX;
            public float ParalaxEffectY;
            [HideInInspector] public Vector2 StartPosition;
            [HideInInspector] public Vector3 StartCameraPosition;
        }
    }
}
