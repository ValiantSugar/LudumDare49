using LevelEntity.Triggers;
using Manager;
using Managers;
using PlayableEntity;
using PlayableEntity.Data;
using UI;
using UI.ParametersMetes;
using UnityEngine;

namespace LevelEntity
{
    public class LevelDirector : MonoBehaviour
    {
        [SerializeField] private Unit[] enemyUnits;
        [SerializeField] private Unit bossUnit;
        [SerializeField] private BossFightTrigger bossFightTrigger;
        [SerializeField] private StatName rewardStatType;
        [SerializeField] private CheckPointTrigger[] checkPoints;
        [SerializeField] private GameObject unstableLocation;
        [SerializeField] private GameObject stableLocation;
        [SerializeField] private ParticleSystem[] bossFogs;
        [SerializeField] private GameObject[] bossWalls;
        [SerializeField] private GameObject wallBeforeNextLevel;
        [SerializeField] private string levelName; // Name of social issue
        [SerializeField] private string audioName; 
        [SerializeField] private bool isBossFight;
        private Unit playerUnit;
        private BossHPView bossHpView;


        private CheckPointTrigger currentCheckPoint;
        private float maxBossHP;

        protected void Start()
        {
            if(playerUnit == null)
                playerUnit = PlayerDontDestroy.Instance.playerUnit;

            playerUnit.OnDie += OnPlayerDeath;

            if (bossUnit != null)
            {
                bossUnit.OnTakeDamage += OnEnemyUnitTakeDamage;
                bossUnit.OnDie += OnBossDeath;
            }

            foreach (var unit in enemyUnits)
                unit.OnTakeDamage += OnEnemyUnitTakeDamage;

            foreach (var checkPoint in checkPoints)
                checkPoint.OnTriggerEnter += SetCurrentCheckPoint;

            if(bossFightTrigger != null)
                bossFightTrigger.OnTriggerEnter += StartBossFight;

            bossHpView = isBossFight ? UIManager.Instance.BossHpView : UIManager.Instance.MiniBossHpView;
            if (isBossFight)
                UIManager.Instance.ShowBossHP();

            SetMaxBossHP();
            if (audioName != null)
            {
                AudioManager.DefaultGameMusicName = audioName;
            }
        }

        private void SetCurrentCheckPoint(CheckPointTrigger checkPoint)
        {
            currentCheckPoint = checkPoint;
            playerUnit.Remember();
            Debug.Log("Set CheckPoint");
        }

        private void StartBossFight()
		{
            bossUnit.PlayableStats.HP.SetMax(maxBossHP);
            foreach (var fog in bossFogs)
            {
                fog.loop = true;
                fog.Play();
            }
            foreach (var wall in bossWalls)
                wall.gameObject.SetActive(true);

            AudioManager.TrySetMusic(AudioManager.BossThemeName);
        }

        private void SetMaxBossHP()
		{
            if (bossUnit == null)
            {
                Debug.LogError("Boss unit has not been found!");
                return;
            }
            maxBossHP = 0;
            maxBossHP += bossUnit.PlayableStats.HP.GetMax();
            foreach (var unit in enemyUnits)
                maxBossHP += unit.PlayableStats.HP.GetMax();

            bossHpView.Init(maxBossHP, levelName);
        }

        private void OnEnemyUnitTakeDamage(float damage)
        {
            maxBossHP -= damage;
            if (maxBossHP < 1)
                maxBossHP = 1;
            bossHpView.UpdateView(maxBossHP);
        }

        private void OnPlayerDeath()
        {
            Hider.Fade(true, onDone: () =>
            {
                playerUnit.Recover();
                currentCheckPoint.MementosRecovery();
                bossFightTrigger.gameObject.SetActive(true);
                foreach (var fog in bossFogs)
                    fog.Stop();
                foreach (var wall in bossWalls)
                    wall.gameObject.SetActive(false);

                SetMaxBossHP();

                bossFightTrigger.Restart();
                AudioManager.TrySetMusic(AudioManager.DefaultGameMusicName);
                Hider.Fade(false);
            });
        }

        private void OnBossDeath()
        {
            switch (rewardStatType)
            {
                case StatName.Block:
                    InstabilityController.Instance.StopInstability(rewardStatType,
                        playerUnit.PlayableStats.BlockAmount);
                    break;
                case StatName.Jump:
                    InstabilityController.Instance.StopInstability(rewardStatType, playerUnit.PlayableStats.JumpForce);
                    break;
                case StatName.Attack:
                    InstabilityController.Instance.StopInstability(rewardStatType, playerUnit.PlayableStats.Damage);
                    break;
            }

            foreach (var fog in bossFogs)
                fog.loop = false;
            foreach (var wall in bossWalls)
                wall.gameObject.SetActive(false);
            
            foreach (var enemyUnit in enemyUnits) Destroy(enemyUnit.gameObject);
            if (unstableLocation != null)
                unstableLocation.SetActive(false);
            if (stableLocation != null)
                stableLocation.SetActive(true);
            EndLevel();
        }

        private void EndLevel()
		{
            AudioManager.TrySetMusic(AudioManager.DefaultGameMusicName);
            wallBeforeNextLevel.gameObject.SetActive(false);
        }
	}
}