﻿using System.Collections.Generic;
using DG.Tweening;
using PlayableEntity;
using PlayableEntity.Data;
using UnityEngine;
using Utilities;

namespace LevelEntity
{
    public class InstabilityController : Singleton<InstabilityController>
    {
        public Unit PlayerUnit;
        [SerializeField] private float TimeToFillStat = 1.5f;
        private readonly Dictionary<StatName, Sequence> instabilitySequences = new Dictionary<StatName, Sequence>();
        private bool isInit = false;

		private void Start()
		{
            FirstInit(PlayerUnit.PlayableStats);
		}

        private void FirstInit(PlayableStatsSO playableStatsSo)
        {
            if (isInit) return;
            StartInstability(playableStatsSo.Damage, StatName.Attack);
            StartInstability(playableStatsSo.BlockAmount, StatName.Block);
            StartInstability(playableStatsSo.JumpForce, StatName.Jump);
        }

        private void StartInstability(FloatStat floatStat, StatName statName)
        {
            Sequence instabilitySequence = DOTween.Sequence();
            floatStat.Set(floatStat.Min);
            instabilitySequence.Append(DOTween.To(floatStat.GetCurrent, floatStat.Set, floatStat.Max,
                TimeToFillStat)).SetEase(Ease.InSine);
            instabilitySequence.Append(DOTween.To(floatStat.GetCurrent, floatStat.Set, floatStat.Min,
                TimeToFillStat)).SetEase(Ease.OutSine);
            instabilitySequence.SetLoops(-1, LoopType.Restart);
            instabilitySequences.Add(statName, instabilitySequence);
        }

        public void StopInstability(StatName statName, FloatStat statToStabilize)
        {
            instabilitySequences[statName].Pause().Kill();
            statToStabilize.Set(statToStabilize.GetStable());
        }
    }
}