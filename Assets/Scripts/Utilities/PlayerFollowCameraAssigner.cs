using Cinemachine;
using PlayableEntity;
using UnityEngine;

[RequireComponent(typeof(CinemachineVirtualCamera))]
public class PlayerFollowCameraAssigner : MonoBehaviour
{
    private void Start()
    {
        TryGetComponent(out CinemachineVirtualCamera cinemachineVirtualCamera);
        cinemachineVirtualCamera.Follow = PlayerDontDestroy.Instance.playerUnit.PlayableView.transform;
    }
}
