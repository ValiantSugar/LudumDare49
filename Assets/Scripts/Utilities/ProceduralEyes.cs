using System;
using System.Collections.Generic;
using PlayableEntity;
using UnityEngine;

//https://github.com/YooPita/ProceduralEyes

public class ProceduralEyes : MonoBehaviour
{
    public bool Preview = true;
    public float MaxZFar = -5;
    [SerializeField] private Transform target;
    [SerializeField] private List<Eye> eyes = new List<Eye>();

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        foreach (Eye a in eyes)
        {
            foreach (var pupil in a.pupils)
                if (pupil != null) Gizmos.DrawWireSphere(pupil.position, a.radius);
        }
    }

    private void AssignPlayerAsTarget()
    {
        target = PlayerDontDestroy.Instance.playerUnit.transform;
    }
    void Update()
    {
        DrawEyes();
    }

    void OnDrawGizmos()
    {
        if (Preview) DrawEyes();
    }

    void DrawEyes()
    {
        if (target == null)
        {
            AssignPlayerAsTarget();
        }
        foreach (Eye a in eyes)
        {
            foreach (var pupil in a.pupils)
            {
                float lerpVal = (MaxZFar - target.position.z) / MaxZFar;
                var controller = Vector2.Lerp(pupil.position, target.position, lerpVal / 2);

                var leftPos = Vector2.ClampMagnitude(controller - (Vector2)pupil.position, a.radius);//Normalization
                if (Quaternion.Angle(pupil.rotation, target.rotation) > 60)
                {
                    leftPos.x *= -1;
                }

                pupil.GetChild(0).localPosition = leftPos;//The first child is the pupil 
            }
        }
    }
}

[System.Serializable]
public struct Eye
{
    public Transform[] pupils;
    public float radius;
}