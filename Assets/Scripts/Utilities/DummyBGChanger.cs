using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    public class DummyBGChanger : MonoBehaviour
    {
        [SerializeField] private GameObject[] bgs;

		private void Awake()
		{
			bgs[Random.Range(0, bgs.Length)].gameObject.SetActive(true);
		}
	}
}
