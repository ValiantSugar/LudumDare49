﻿using System;

namespace Utilities
{
    [Serializable]
    public class FloatStat
    {
        public float Max;
        public float Min;
        public float Current;
        public float Stable;
        public event Action<float> OnChange;
        public bool IsMax => Current.Equals(Max);

        public FloatStat(float max = default, float current = default, float min = default, float stable = default)
        {
            Max = max;
            Min = min;
            Current = current;
            Stable = stable;
        }

        #region Getters

        public float GetMax() => Max;
        public float GetMin() => Min;
        public float GetStable() => Stable;
        public float GetCurrent() => Current;

        #endregion

        public void Reset()
		{
            Current = Max;
            OnChange?.Invoke(Current);
        }

        public void SetMax(float value)
		{
            Max = value;
            Current = value;
            OnChange?.Invoke(value);
        }
        
        public void SetCurrentToMax()
        {
            Current = Max;
            OnChange?.Invoke(Current);
        }

        public void Set(float value)
        {
            Current = value;
            OnChange?.Invoke(value);
        }

        public FloatStat GetClone()
		{
            return new FloatStat(Max, Current, Min, Stable);
		}
    }
}